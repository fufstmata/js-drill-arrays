
// VERY GOOD recursive problem

// I wasn't declaring final array outside, which cause lot of problems
const final = [];
function flatten(elements, depth) {
    if(Array.isArray(elements) && depth === undefined){
        let depth = 1
        elements.forEach( current => {
            if(Array.isArray(current) && depth >= 1) {
                // Most crucial line here, earlier I was using a count variable, but it always got reset
                // since it was declared inside the function. Key is to set it outside.
                flatten(current, depth-1);
            } else {
                final.push(current);
            }
        });
    } else if (Array.isArray(elements) && Number.isFinite(depth)) {
        elements.forEach( current => {
            if(Array.isArray(current) && depth >= 1) {
                // Most crucial line here, earlier I was using a count variable, but it always got reset
                // since it was declared inside the function. Key is to set it outside.
                flatten(current, depth-1);
            } else {
                final.push(current);
            }
        });
    }
    return final;
}

module.exports = flatten;