/* Aim of this problem is to try and replicate the function definition of forEach as it might be in ES5 documentation.

forEach accepts a lot of arguments like so :-
forEach(callbackfn: (value: T, index: number, array: T[]) => void, thisArg?: any): void;

value depicts the current value of element under observation during any iteration, index represents current index value,
and so on and so forth.

This definition accepts func as argument which is basically a function definition. For solving this problem, the definition
has been provided in testEach.js file (aka the callback function). This type of documenting makes it clear and obvious, that
forEach documentation won't have the callback function written alongside it since they are two independent entities.

*/
const potential = []
function each(elements, callBackFunction) {
    if(Array.isArray(elements) && each.arguments.length === 2){
        const maxL = elements.length;
        // Nice usage of .name property to identify if I can use the function for printing purpose or anything else
        if(callBackFunction.name === 'printing'){   // callBackFunction name might cause conflict for testing with bot. ASK about this
            let start = 0;
        while(start < maxL) {
            callBackFunction(elements,start);
            start += 1;

            }
            return undefined;
        } else {
            let start = 0;
            while(start < maxL) {
                potential.push(callBackFunction(elements,start));
                start += 1;
                }
            return potential;
        }
    } else {
        console.log(undefined);     // Should this be [] or "undefined" still ??
    }
}


module.exports = each;