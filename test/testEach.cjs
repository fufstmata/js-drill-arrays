/* 
Confirm doubt with sir about testing parameters since my function is trying to
replicate the behavior of forEach in which there is only a console output and the 
return value of forEach is always "undefined" . So should I stick that and if so,
how should I test my function. With using return, the test becomes straightforward.

IMPORTANT NOTE: had to let go of this approach, due to time constraint. Main point was
to understand how the functionatlity can be implemented
*/

const items = [1, 2, 3, 4, 5, 5];
const assert = require('assert');
const each = require('../each.cjs');

function printing(array,indexNumber) {
    const ans = [];
    ans.push(array[indexNumber], indexNumber);
    console.log(...ans,array);
}

const final = each(items, printing);
console.log(final);        // Returning undefined as intended (replicating how forEach works)





