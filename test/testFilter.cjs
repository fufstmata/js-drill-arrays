const items = [1, 2, 3, 4, 5, 5];
const assert = require('assert');
const testFilter = require('../filter.cjs');

function callback(elementValue,index,elements) {
    return elementValue % 2 === 0;
}

const test = testFilter(items, callback);

const expected = [2,4];

assert.deepStrictEqual(test,expected);