const items = [1, 2, 3, 4, 5, 5];
const assert = require('assert');
const mapTest = require('../map.cjs');
const each = require('../each.cjs');


function testMutation(elementValue, index, array) {
    mutation = elementValue*2;
    return mutation;
}

const test = mapTest(items, testMutation);
const expected = [2,4,6,8,10,10];
assert.deepStrictEqual(test,expected);