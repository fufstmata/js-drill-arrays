const items = [1, [2], [[3]], [[[4]]]];
const assert = require('assert');
const testFlatten = require('../flatten.cjs');

const final = testFlatten(items,3);

const expected = [1,2,3,4];
console.log(final);
assert.deepStrictEqual(final, expected);