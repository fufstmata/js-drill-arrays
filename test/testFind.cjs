const items = [1, 2, 3, 4, 5, 5];
const assert = require('assert');
const testFind = require('../find.cjs');

function callback(elementValue, searchValue) {
    return elementValue === searchValue;
}

const final = testFind(items, callback, 0);

const expected = undefined;

assert.deepStrictEqual(final, undefined);


