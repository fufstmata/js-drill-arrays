const items = [1, 2, 3, 4, 5, 5];


function find(elements, callback, searchValue) {
    if(!Array.isArray(elements)) {
        return undefined;
    } else {
        const maxLength = elements.length;
        let flag = false;
        for(let index = 0; index < maxLength; index++) {
            flag = callback(elements[index], searchValue)
            if(flag === true) {
                return elements[index]
            }
        }
        return undefined;
    }

}


module.exports = find;