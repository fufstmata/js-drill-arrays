
/* 
Ask about this approach if you can use each function and then use this
in conjunction with callBackFunction for map to mutate the array in place
*/
const items = [1, 2, 3, 4, 5, 5];

const pleaseWork = []

function map(elements, callBackFunction) {
    if(Array.isArray(elements) && map.arguments.length === 2) {
        const maxLength = elements.length;
        for(let index = 0; index < maxLength; index++){
            if(callBackFunction.name === 'parseInt'){
                pleaseWork.push(callBackFunction(elements[index], index)); 
            } else {
                pleaseWork.push(callBackFunction(elements[index], index, elements));
            }
            
        }
        return pleaseWork;
    } else {
        return(pleaseWork);
    }
}

module.exports = map;
