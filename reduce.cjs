
const items = [1, 2, 3, 4, 5, 5];
// const each = require('./each.cjs');
// No need to import each function
// Just stick to using forEach and completing the testing phase.

function reduce(elements, callBackFunction, startValue) {
    if(!Array.isArray(elements) && !reduce.arguments.length < 2 ) {
        return([]);
    } else if (startValue === undefined){
        let startValue = elements[0];
        const maxL = elements.length;
        for(let index = 1; index < maxL ; index++) {
            startValue = callBackFunction(startValue,elements[index],index,elements);
        }
        return startValue;
    } else {
        const maxL = elements.length;
        for(let index = 0; index < maxL ; index++) {
            startValue = callBackFunction(startValue,elements[index],index,elements);
        }
        return startValue;
    }
}

module.exports = reduce;

